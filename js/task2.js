// ver 1 - Functional   
const books = [
    { 
      author: "Скотт Бэккер",
      name: "Тьма, что приходит прежде",
      price: 70 
    }, 
    {
     author: "Скотт Бэккер",
     name: "Воин-пророк",
    }, 
    { 
      name: "Тысячекратная мысль",
      price: 70
    }, 
    { 
      author: "Скотт Бэккер",
      name: "Нечестивый Консульт",
      price: 70
    }, 
    {
     author: "Дарья Донцова",
     name: "Детектив на диете",
     price: 40
    },
    {
     author: "Дарья Донцова",
     name: "Дед Снегур и Морозочка",
    }
  ];

//   bookHandler(books)

  function bookHandler(books)
  {
      if(typeof books === 'object' && books.length > 0)
      {
        let parent = document.getElementById('root'),
            ul = document.createElement('ul')
        ul.className = 'result-list'

        books.forEach((boo,index) => {
            try
            {
                if(!boo['author'])
                {
                    throw new Error(`В объекте № ${index + 1} не указан автор!`)
                }
                if(!boo['name'])
                {
                    throw new Error(`В объекте № ${index + 1} не указано название!`)
                }
                if(!boo['price'])
                {
                    throw new Error(`В объекте № ${index + 1} не указана цена!`)
                }

                let {author,name:title,price = 0} = boo

                //insert to ul
                ul.insertAdjacentHTML('beforeend',`<li class="result-list__element">Название: "${title}", Автор: ${author}, Цена: ${price}</li>`)
            }
            catch(err)
            {
                console.error(err.message)
            }
            //insert to div
            parent.insertAdjacentElement('beforeend',ul)          
        })
      }
  }


  

  //ver 2 - OOP
  class Books
  {
    books = [
        { 
          author: "Скотт Бэккер",
          name: "Тьма, что приходит прежде",
          price: 70 
        }, 
        {
         author: "Скотт Бэккер",
         name: "Воин-пророк",
        }, 
        { 
          name: "Тысячекратная мысль",
          price: 70
        }, 
        { 
          author: "Скотт Бэккер",
          name: "Нечестивый Консульт",
          price: 70
        }, 
        {
         author: "Дарья Донцова",
         name: "Детектив на диете",
         price: 40
        },
        {
         author: "Дарья Донцова",
         name: "Дед Снегур и Морозочка",
        }
    ]
    parent = document.getElementById('root')
    list = {
        tag: 'ul',
        className: 'result-list',

    }

    render()
    {
        if(typeof this.books === 'object' && this.books.length > 0)
        {
            let ul = {},
                {tag,classes} = this.list
            ul = document.createElement(tag)
            ul.className = classes

            books.forEach((boo,index) => {
                try
                {
                    if(!boo['author'])
                    {
                        throw new Error(`В объекте № ${index + 1} не указан автор!`)
                    }
                    if(!boo['name'])
                    {
                        throw new Error(`В объекте № ${index + 1} не указано название!`)
                    }
                    if(!boo['price'])
                    {
                        throw new Error(`В объекте № ${index + 1} не указана цена!`)
                    }

                    let {author,name:title,price = 0} = boo

                    //insert to ul
                    ul.insertAdjacentHTML('beforeend',`<li class="result-list__element">Название: "${title}", Автор: ${author}, Цена: ${price}</li>`)
                }
                catch(err)
                {
                    console.error(err.message)
                }
                //insert to div
                this.parent.insertAdjacentElement('beforeend',ul)          
            })
        }
    }
}

let booksObj = new Books
  booksObj.render()